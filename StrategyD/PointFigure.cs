﻿using System.Collections.Generic;

namespace StrategyD
{
    class PointFigure
    {
        enum Action
        {
            Buy = 1,
            DoNothing = 0,
            Sell = -1
        }

        enum Direction
        {
            Up,
            Down
        }

        private readonly int MAX_HISTORY;
        private double box;
        private readonly double reversal;
        private Action action;
        private Direction direction;

        private readonly List<double> prices;
        private readonly List<int> columns;

        private int i;
        private int columnIndex;
        private int startIndex;
        private double lastBox;

        private int calculateColumns()
        {
            for (i = 0; i < prices.Count; i++)
            {
                columns[i] = 0;
            }

            lastBox = prices[0];

            for (i = 1; i < prices.Count; i++)
            {
                if (prices[i] - lastBox >= reversal*box)
                {
                    do
                    {
                        if (lastBox + box > prices[i])
                        {
                            break;
                        }
                        lastBox += box;
                        columns[i]++;
                        onColumnChange();
                    } while (true);
                    direction = Direction.Up;
                    startIndex = i + 1;
                    break;
                } else if (lastBox - prices[i] >= reversal*box)
                {
                    do
                    {
                        if (lastBox - box < prices[i])
                        {
                            break;
                        }
                        lastBox -= box;
                        columns[i]--;
                        onColumnChange();
                    } while (true);
                    direction = Direction.Down;
                    startIndex = i + 1;
                    break;
                }
                else
                {
                }
            }

            columnIndex = 1;

            updateColumns();

            return columnIndex;
        }

        private void updateColumns()
        {
            for (i = startIndex; i < prices.Count; i++)
            {
                if (direction == Direction.Up)
                {
                    if (prices[i] - lastBox >= box)
                    {
                        do
                        {
                            if (lastBox + box > prices[i])
                            {
                                break;
                            }
                            lastBox += box;
                            columns[columnIndex]++;
                            onColumnChange();
                        } while (true);
                    } else if (lastBox - prices[i] >= reversal*box)
                    {
                        increaseColumns();
                        columnIndex++;
                        do
                        {
                            if (lastBox - box < prices[i])
                            {
                                break;
                            }
                            lastBox -= box;
                            columns[columnIndex]--;
                            onColumnChange();
                        } while (true);
                        direction = Direction.Down;
                    }
                } else if (direction == Direction.Down)
                {
                    if (lastBox - prices[i] >= box)
                    {
                        do
                        {
                            if (lastBox - box < prices[i])
                            {
                                break;
                            }
                            lastBox -= box;
                            columns[columnIndex]--;
                            onColumnChange();
                        } while (true);
                    } else if (prices[i] - lastBox >= reversal*box)
                    {
                        increaseColumns();
                        columnIndex++;
                        do
                        {
                            if (lastBox + box > prices[i])
                            {
                                break;
                            }
                            lastBox += box;
                            columns[columnIndex]++;
                            onColumnChange();
                        } while (true);
                        direction = Direction.Up;
                    }
                }
            }

            startIndex = i;
        }

        private void increaseColumns()
        {
            if (columns.Count > 0)
            {
                onNewColumn();
            }

            columns.Add(0);
        }

        private void removePrices(int howMany)
        {
            if (prices.Count - howMany > 2)
            {
                prices.RemoveRange(0, howMany);
                startIndex -= howMany;
            }
        }

        private void removeColumns(int howMany)
        {
            if (columns.Count - howMany > 2)
            {
                columns.RemoveRange(0, howMany);
                columnIndex -= howMany;
            }
        }

        private void onNewColumn()
        {
            if (columns.Count >= 3)
            {
                int a = columns[columns.Count - 3];
                int b = columns[columns.Count - 2];
                int c = columns[columns.Count - 1];

                if (a + (b - 1) + (c + 1) > a + 3 && c > 0)
                {
                    action = Action.Buy;
                } else if (a + (b + 1) + (c - 1) < a - 3 && c < 0)
                {
                    action = Action.Sell;
                }
                else
                {
                    action = Action.DoNothing;
                }
            }
        }

        private void onColumnChange()
        {
        }

        public PointFigure(double boxSize, double reversalAmount)
        {
            MAX_HISTORY = 60;
            box = boxSize;
            reversal = reversalAmount;
            prices = new List<double>();
            columns = new List<int>();
            i = 0;
            columnIndex = 0;
            startIndex = 0;
            lastBox = 0;
            direction = Direction.Up;
            action = Action.DoNothing;
        }

        public void update(double tick)
        {
            prices.Add(tick);
            if (prices.Count == 2)
            {
                increaseColumns();
                increaseColumns();
                calculateColumns();
            } else if (prices.Count > 2)
            {
                updateColumns();
            }

            if (prices.Count > MAX_HISTORY)
            {
                removePrices(MAX_HISTORY/2);
            }

            if (columns.Count > MAX_HISTORY)
            {
                removeColumns(MAX_HISTORY / 2);
            }
        }

        public int getAction()
        {
            Action result = action;
            action = Action.DoNothing;
            return (int) result;
        }

        public void setBoxSize(double boxSize)
        {
            box = boxSize;
        }

        public double getBoxSize()
        {
            return box;
        }

        public void blankOut()
        {
            prices.Clear();
            columns.Clear();
            box = 1000.0;
            direction = Direction.Down;
            action = Action.DoNothing;
            i = 0;
            columnIndex = 0;
            startIndex = 0;
            lastBox = 0;
        }
    }
}
