﻿using System.Collections.Generic;
using System.Linq;

namespace StrategyD
{
    class OrderEngine
    {
        private readonly List<Order> orders;
        private readonly double minSpread;
        private double spread;
        private int count;
        public OrderEngine(double minSpread)
        {
            orders = new List<Order>();
            this.minSpread = minSpread;
            spread = 0.0d;
            count = 0;
        }

        public void update(double bid, double offer)
        {
            bool changed = false;
            spread = offer - bid;
            foreach (var order in orders)
            {
                if (order.getStatus() == Order.Status.Open)
                {
                    order.update(bid, offer);
                    if (order.getStatus() != Order.Status.Open)
                    {
                        changed = true;
                    }
                }
            }

            if (orders.Count > 100)
            {
                orders.RemoveRange(0, orders.Count - 100);
            }
        }

        public void addOrder(Order.Action action, double open, double tp, double sl)
        {
            if (spread < minSpread)
            {
                var order = new Order(action, open, tp, sl);
                orders.Add(order);
                count++;
            }
        }

        public void blankOut()
        {
            orders.Clear();
        }

        public int getOrderCount()
        {
            return count;
        }
    }

    class Order
    {
        public enum Action
        {
            Buy = 1,
            Sell = -1
        }

        public enum Status
        {
            Open,
            Profit,
            Loss
        }

        private readonly Action action;
        private readonly double open, tp, sl;
        private Status status;

        public Order(Action action, double open, double tp, double sl)
        {
            this.action = action;
            this.open = open;
            this.tp = tp;
            this.sl = sl;
        }

        public void update(double bid, double offer)
        {
            if (status == Status.Open)
            {
                if (action == Action.Buy)
                {
                    if (offer >= tp)
                    {
                        status = Status.Profit;
                    }
                    else if (offer <= sl)
                    {
                        status = Status.Loss;
                    }
                    else
                    {
                        status = Status.Open;
                    }
                }
                else if (action == Action.Sell)
                {
                    if (bid <= tp)
                    {
                        status = Status.Profit;
                    }
                    else if (bid >= sl)
                    {
                        status = Status.Loss;
                    }
                    else
                    {
                        status = Status.Open;
                    }
                }
            }
        }

        public Action getAction()
        {
            return action;
        }

        public double getOpen()
        {
            return open;
        }

        public double getTP()
        {
            return tp;
        }

        public double getSL()
        {
            return sl;
        }

        public Status getStatus()
        {
            return status;
        }
    }
}
