﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using RGiesecke.DllExport;

namespace StrategyD
{
    public class Main
    {
        private static List<PointFigure> charts;
        private static List<OrderEngine> engines;

        [DllExport("createTool", CallingConvention = CallingConvention.StdCall)]
        public static int createTool(double box, double reversal, double minSpread)
        {
            var pf = new PointFigure(box, reversal);
            var e = new OrderEngine(minSpread);
            if (charts == null)
            {
                charts = new List<PointFigure>();
            }

            if (engines == null)
            {
                engines = new List<OrderEngine>();
            }
            charts.Add(pf);
            engines.Add(e);
            return charts.Count - 1;
        }

        [DllExport("newTick", CallingConvention = CallingConvention.StdCall)]
        public static int newTick(int handle, double bid, double offer)
        {
            double tick = (bid + offer) / 2.0;
            charts[handle].update(tick);
            engines[handle].update(bid, offer);
            int action = charts[handle].getAction();
            if (action == 1)
            {
                engines[handle].addOrder(Order.Action.Buy, offer, offer + (3.0 * charts[handle].getBoxSize()), offer - (2.0 * charts[handle].getBoxSize()));
            }
            else if (action == -1)
            {
                engines[handle].addOrder(Order.Action.Sell, bid, bid - (3.0 * charts[handle].getBoxSize()), bid + (2.0 * charts[handle].getBoxSize()));
            }
            else
            {

            }
            return action;
        }

        [DllExport("setBox", CallingConvention = CallingConvention.StdCall)]
        public static void setBox(int handle, double boxSize)
        {
            charts[handle].setBoxSize(boxSize);
        }

        [DllExport("getBox", CallingConvention = CallingConvention.StdCall)]
        public static double getBox(int handle)
        {
            return charts[handle].getBoxSize();
        }

        [DllExport("cleanEverything", CallingConvention = CallingConvention.StdCall)]
        public static void cleanEverything()
        {
            charts.Clear();
            engines.Clear();
        }

        [DllExport("cleanTool", CallingConvention = CallingConvention.StdCall)]
        public static void cleanTool(int handle)
        {
            charts[handle].blankOut();
            engines[handle].blankOut();
        }

        [DllExport("getOrderCount", CallingConvention = CallingConvention.StdCall)]
        public static int getOrderCount(int handle)
        {
            return engines[handle].getOrderCount();
        }
    }
}
