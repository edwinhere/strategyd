//+------------------------------------------------------------------+
//|                                                    StrategyA.mq4 |
//|                           Copyright 2012, Edwin Jose Palathinkal |
//|                                   http://meinwords.wordpress.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2012, Edwin Jose Palathinkal"
#property link      "http://meinwords.wordpress.com"

#define MAX 9

#import "StrategyD.dll"
   int createTool(double box, double reversal, double minSpread);
   int newTick(int handle, double bid, double offer);
   void setBox(int handle, double box);
   double getBox(int handle);
   void cleanEverything();
   void cleanTool(int handle);
   int getOrderCount(int handle);
#import

#import "ole32.dll"
   int CoCreateGuid(int & Bytes[]);
#import

#include <WinUser32.mqh>
#import "user32.dll"
  int GetForegroundWindow();
#import

int dllHandle;
double box = 0.0012;
int previousAction = -2;
int ticket = -1;
double lots = 0.0;
int magic = 0;
string comment;
string server;
bool demo;
string message;
bool active;

extern double constant = 0.0;
extern double MINLOT = 0.1;
extern double FRACTION = 0.02;

/*

Symbol	Timeframe	Scaler	PF	   Trades	Index (PF - 1) * Trades

EURUSD	M30	      1.0	   1.30	544	   163.2
EURJPY	M30	      0.8	   1.47	322	   151.3
AUDUSD	M30	      1.0	   1.54	269	   145.3
USDJPY	M30	      1.3	   1.47	289	   135.8
--
EURCHF	M15	      1.4	   2.09	100	   109.0
EURGBP	M30	      0.9	   1.17	529	   089.9
--
USDCAD	M30	      0.9	   1.21	272	   057.1
CADJPY	M15	      1.5	   1.56	100	   056.0
AUDJPY	M15	      1.0	   1.19	211	   040.1
GBPUSD	M30	      0.7	   1.06	537	   032.2
USDCHF	M30	      2.0	   1.32	063	   020.2
GBPJPY	M15	      0.9	   1.15	108	   016.2
CHFJPY	M15	      1.2	   1.10	114	   011.4

*/
 
//+------------------------------------------------------------------+
 
int curIndex;
datetime times[MAX];
 
//+------------------------------------------------------------------+
 
int start;
 
int init () {
  server = AccountServer();
  Print("Server: " + server);
  Print("Account: " + AccountName());
  demo = IsTesting() || IsVisualMode();
  active = false;
  magic = GenerateMagicNumber();
  Print("Magic: " + magic);
  comment = WindowExpertName() + Symbol() + Period();
  Print("Comment: " + comment);
  box = calculateBoxSize();
  dllHandle = createTool(box, 1.0, 10 * Point);
  Print("B: ", getBox(dllHandle));
  curIndex = utils.periodToPeriodIndex(Period());
  times[curIndex] = Time[0];
  for(int i=curIndex+1; i<MAX; i++)
    times[i] = times[curIndex]- MathMod(times[curIndex],utils.periodIndexToPeriod(i)*60);
 
  return(0);
}
 
//+------------------------------------------------------------------+
 
int deinit() {
  if(GlobalVariableCheck("_Trade")) GlobalVariableDel("_Trade");
  Print("Order Count: " + getOrderCount(dllHandle));
  cleanTool(dllHandle);
  return (0);
}
 
//+------------------------------------------------------------------+
 
int start() {
  if (times[curIndex] != Time[0]) {
    times[curIndex] = Time[0];
    onBar(Period());
    for(int i=curIndex+1; i<MAX; i++) {
      int period  = utils.periodIndexToPeriod(i),
          seconds = period*60,
          time0   = times[curIndex] - MathMod(times[curIndex],seconds);
      if (times[i] != time0) {
        times[i] = time0;
        onBar(period);
      }
    }
  }
 
  onTick();
 
  return(0);
}
 
int utils.periodToPeriodIndex(int period) {
  switch(period) {
    case PERIOD_M1  : return(0); break;
    case PERIOD_M5  : return(1); break;
    case PERIOD_M15 : return(2); break;
    case PERIOD_M30 : return(3); break;
    case PERIOD_H1  : return(4); break;
    case PERIOD_H4  : return(5); break;
    case PERIOD_D1  : return(6); break;
    case 20         : return(7); break;
    case 55         : return(8); break;
  }
}
 
int utils.periodIndexToPeriod(int index) {
  switch(index) {
    case 0: return(PERIOD_M1); break;
    case 1: return(PERIOD_M5); break;
    case 2: return(PERIOD_M15); break;
    case 3: return(PERIOD_M30); break;
    case 4: return(PERIOD_H1); break;
    case 5: return(PERIOD_H4); break;
    case 6: return(PERIOD_D1); break;
    case 7: return(20); break;
    case 8: return(55); break;
  }
}
 
//+------------------------------------------------------------------+
// Code
//+------------------------------------------------------------------+
 
void onTick() {
  if(box == 0.0) {
    return;
  }
  
  double normalizedTick = (Ask + Bid) / 2.0;
  int action = newTick(dllHandle, Bid, Ask);
  double spread = Ask - Bid;
  
  if(action != 0 && spread < 10 * Point) {
    active = OrdersOpen(magic) > 0;
    if(!active) {
      if(action == 1) {
        lots = Lots("", FRACTION, 0);
        if(lots < MINLOT) lots = MINLOT;
        if(OrderCheckFunds(Symbol(), OP_BUY, lots)) {
          ticket = OrderSend(Symbol(), OP_BUY, lots, Ask, 1, 0, 0, comment, magic);
          if(ticket == -1) {
            if(demo)
              Print(ErrorDescription2(GetLastError2()));
            else
              SendNotification(ErrorDescription2(GetLastError2()));
          } else {
            if(OrderSelect(ticket, SELECT_BY_TICKET)) {
              OrderModify(ticket, OrderOpenPrice(), Ask - NormalizeDouble(box * 2.0, Digits), Bid + NormalizeDouble(box * 3.0, Digits), 0);
              PauseTest();
            }
            message = "Bought " + lots + " " + Symbol() + "@" + Ask;
            if(demo)
              Print(message);
            else
              SendNotification(message);
          }
        } else {
          if(demo)
            Print("Tried to BUY from " + comment + " but no cash!");
          else
            SendNotification("Tried to BUY from " + comment + " but no cash!");
        }
      } else if(action == -1) {
        lots = Lots("", FRACTION, 0);
        if(lots < MINLOT) lots = MINLOT;
        if(OrderCheckFunds(Symbol(), OP_SELL, lots)) {
          ticket = OrderSend(Symbol(), OP_SELL, lots, Bid, 1, 0, 0, comment, magic);
          if(ticket == -1) {
            if(demo)
              Print(ErrorDescription2(GetLastError2()));
            else
              SendNotification(ErrorDescription2(GetLastError2()));
          } else {
            if(OrderSelect(ticket, SELECT_BY_TICKET)) {
              OrderModify(ticket, OrderOpenPrice(), Bid + NormalizeDouble(box * 2.0, Digits), Ask - NormalizeDouble(box * 3.0, Digits), 0);
              PauseTest();
            }
            message = "Sold " + lots + " " + Symbol() + "@" + Bid;
            if(demo)
              Print(message);
            else
              SendNotification(message);
          }
        } else {
          if(demo)
            Print("Tried to SELL from " + comment + " but no cash!");
          else
            SendNotification("Tried to SELL from " + comment + " but no cash!");
        }
      }
      previousAction = action;
    } else {
      if(previousAction != action && previousAction != -2) {
        if(previousAction == 1)
          OrderClose(ticket, lots, Bid, 5);
        else if(previousAction == -1)
          OrderClose(ticket, lots, Ask, 5);
      }
      previousAction = action;
    }
  }
}
 
//+------------------------------------------------------------------+
 
void onBar(int period) {
   if(period != PERIOD_H1) {
      return;
   }
   
   active = OrdersOpen(magic) > 0;
   
   if(active) {
     return;
   }
   
   box = calculateBoxSize();
   setBox(dllHandle, box);
   if(!demo)
     Print("B: ", getBox(dllHandle));
}
 
//+------------------------------------------------------------------+

double calculateBoxSize() {
   string symbol = Symbol();
   if(symbol == "EURUSD") {
      return (iATR(symbol, PERIOD_M30, 24, 0) * 1.0); //1.30 from 544 trades
   } else if(symbol == "GBPUSD") {
      return (iATR(symbol, PERIOD_M30, 24, 0) * 0.7); //1.06 from 537 trades
   } else if(symbol == "USDCHF") {
      return (iATR(symbol, PERIOD_M30, 24, 0) * 2.0); //1.32 from 63 trades
   } else if(symbol == "USDJPY") {
      return (iATR(symbol, PERIOD_M30, 24, 0) * 1.3); //1.47 from 289 trades
   } else if(symbol == "AUDUSD") {
      return (iATR(symbol, PERIOD_M30, 24, 0) * 1.0); //1.54 from 269 trades
   } else if(symbol == "EURGBP") {
      return (iATR(symbol, PERIOD_M30, 24, 0) * 0.9); //1.17 from 529 trades
   } else if(symbol == "USDCAD") {
      return (iATR(symbol, PERIOD_M30, 24, 0) * 0.9); //1.21 from 272 trades
   } else if(symbol == "EURCHF") {
      return (iATR(symbol, PERIOD_M15, 24, 0) * 1.4); //2.09 from 100 trades
   } else if(symbol == "EURJPY") {
      return (iATR(symbol, PERIOD_M30, 24, 0) * 0.8); //1.47 from 322 trades
   } else if(symbol == "GBPJPY") {
      return (iATR(symbol, PERIOD_M15, 24, 0) * 0.9); //1.15 from 108 trades
   } else if(symbol == "CHFJPY") {
      return (iATR(symbol, PERIOD_M15, 24, 0) * 1.2); //1.10 from 114 trades
   } else if(symbol == "AUDJPY") {
      return (iATR(symbol, PERIOD_M15, 24, 0) * 1.0); //1.19 from 211 trades
   } else if(symbol == "CADJPY") {
      return (iATR(symbol, PERIOD_M15, 24, 0) * 1.5); //1.56 from 100 trades.
   }
   
   return (0);
}

int GenerateMagicNumber()
{
   // Generate a 16-byte GUID
   int Bytes[4];
   CoCreateGuid(Bytes);
   
   // Hash the GUID using djb2
   int iHash = 5381;
   for (int i = 0; i < 4; i++) {
      //original C code: hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
      iHash = ((iHash << 5) + iHash) + Bytes[i];
   }
   return (MathAbs(iHash));
}

void PauseTest(){   datetime now = TimeCurrent();   static datetime onePerTick;
    if (IsTesting() && IsVisualMode() && IsDllsAllowed() && onePerTick != now){
        for(int i=0; i<100000; i++){        // Delay required for speed=32 (max)
            int main = GetForegroundWindow();               onePerTick = now;
            if (i==0) PostMessageA(main, WM_COMMAND, 0x57a, 0); // 1402. Pause
    }   }
}//*/

//+------------------------------------------------------------------+
//|                                                  OrdersSuite.mq4 |
//|                                          Copyright � 2008, sxTed |
//|                                               sxted@talktalk.net |
//| Purpose.: Functions for processing the orders of Spot currency   |
//|           pairs, Spot Gold and Spot Silver with error handling.  |
//| ThankYou: Big thank you to Professor's Slawa and Stringo for the |
//|           little gems of information on coding, to Rosh and all  |
//|           the moderators for their help and patience and to the  |
//|           members of the forums who give so freely of their code |
//|           and time, from whom I have borrowed quite a bit.       |
//|           Thank you Rasoul for urging me on, hope it was worth   |
//|           waiting for.                                           |
//| Notes...: This library of functions is based on testing with     |
//|           MetaQuotes-Demo, MIG-Demo and InterbankFX-Demo only,   |
//|           and as Slawa stressed each broker's server processes   |
//|           orders slightly differently, like when a pending order |
//|           is changed to a market order it is given a new number, |
//|           so test, test the functions, testing should also be    |
//|           carried out in a fast moving market (just after news   |
//|           or a report is announced) or when it is crawling like  |
//|           just before the week end close.                        |
//|           To use the error handler the MT4 ordering functions    |
//|           OrderSend(), OrderClose(), OrderDelete(), OrderModify()|
//|           and OrderCloseBy() are to be edited in the expert by   |
//|           adding the suffix 2, for example: replace OrderClose() |
//|           with OrderClose2().                                    |
//|           ********************************************************
//|           * OrderProcess() function which dispatches the orders  *
//|           * and handles errors will when a critical error occur, *
//|           * attempt to close ALL market orders and cancel ALL    *
//|           * pending orders including ALL orders placed manually. *
//|           ********************************************************
//|           GetLastError() where it is used in your EA to obtain   |
//|           the last occured error with the MT4 ordering functions |
//|           (as listed above) is to be replaced either with the    |
//|           GetLastError2() function or by refering to the value   |
//|           of the global variable giError.                        |
//|           Use the OrderError() function to print the error.      |
//|           The error handler in OrderProcess() caters for trading |
//|           both currencies and commodities with different trading |
//|           hours simultaneously, so should for example: "XAUUSD"  |
//|           (Spot Gold) be requested to be closed from a script,   |
//|           while the daily close of the exchange is taking place, |
//|           then severe error 133 ERR_TRADE_DISABLED is avoided so |
//|           as to allow the following request from the script to   |
//|           close "EURUSD" (which would not cause the error).      |
//|           Edit or add to the Sessions array with the trading     |
//|           hours of your broker's server and daily close times of |
//|           Spot Gold and Spot Silver commodities.                 |
//|           If your expert does not execute, then hit F3 to access |
//|           the list of Global Variables and change the value of   |
//|           <_StopTrading> to 0 (zero), which was switched ON due  |
//|           to an error encountered in the previous trade.         |
//| WishList: Slawa it would be convenient to have a library init()  |
//|           which is initialised before the init() of an EA/script |
//|           and a library deinit() which is called before executing|
//|           the deinit() of the EA/script (example for removing a  |
//|           semaphore lock in the limited time before the EA is    |
//|           closed down).                                          |
//| Revision: 1 2008.03.13:                                          |
//|             OrderModify2() corrected for omission of dLots when  |
//|             calling OrderProcess().                              |
//|             OrderProcess() error 130 for ORD_MODIFY corrected to |
//|             pass dLots to ORD_CLOSE; error documenting enhanced. |
//|             OrderSend2() is cancelled if the market is volatile. |
//|           2 2008.12.16:                                          |
//|             OrderModify2() corrected to filter out ERR_NO_RESULT.|
//|             OrderProcess() tries to modify the order in 2 more   |
//|             attempts when error 145 ERR_TRADE_MODIFY_DENIED is   |
//|             encountered, after which it closes/cancels the order.|
//|             OrderProcess() retry attempts increased to ten.      |
//|             OrderProcess() added semaphore locking and caters for|
//|             batch processing, closes all market & pending orders |
//|             if a critical error is encountered.                  | 
//|             Lots(), OrderCloseByRetracement(), OrderModifyTS(),  |
//|             OrderSendI() added.                                  |
//|             OrdersOpen() added optional parameter "iExpertID".   | 
//+------------------------------------------------------------------+
#property copyright "Copyright � 2008, sxTed"
#property link      "sxted@talktalk.net"
 
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
#define _MILLION                      1000000                       // avoid mistyping so many zeroes!
//-------------------------------------------------------------------- Action codes in processing orders:                            
#define ACT_SUCCESS                         0                       // Success in processing the order
#define ACT_CAN_RETRY                      -1                       // Remedial action can be taken before
                                                                    // retrying to process the order    
#define ACT_EXIT_START                     -2                       // error requires that start() function be exited  
#define ACT_WAIT_FOR_SESSION               -3                       // avoid error 133 when trading currencies
                                                                    // and commodities at the same time
#define ACT_EXIT_EA                        -4                       // Program logic/System failure,
                                                                    // the expert should be terminated           
//-------------------------------------------------------------------- Color constants
#define CLR_DEF                             0                       // Indicates default gcColorScheme[] to be used 
//-------------------------------------------------------------------- Error codes
#define ERROR                              -1                       // 
#define ERR_146_MAX_TRIES                  10                       // ERR_TRADE_CONTEXT_BUSY maximum retries
#define ERR_146_MIN_SEC                    10                       // ERR_TRADE_CONTEXT_BUSY random delay min
#define ERR_146_MAX_SEC                    30                       // ERR_TRADE_CONTEXT_BUSY random delay max
//-------------------------------------------------------------------- Option() or Options[]
#define OPT_ORDER_RETRY_ATTEMPTS            0                       // index position in array Options[]
#define OPT_PRINT_MSG                       1                       // index position in array Options[]
#define OPT_TRADE_PAUSE_SECONDS             2                       // index position in array Options[]
//-------------------------------------------------------------------- Order processing:
#define ORD_CLOSE                           6                       // Request emanated from OrderClose2()
#define ORD_CLOSEBY                         7                       // Request emanated from OrderCloseBy2()
#define ORD_DELETE                          8                       // Request emanated from OrderDelete2()
#define ORD_MODIFY                          9                       // Request emanated from OrderModify2()
//-------------------------------------------------------------------- Semaphore
#define SEM_ID                "_ThreadUsedBy"                       // name for the global variable
#define SEM_PAUSE                           2                       // number of seconds to pause in between trades
//-------------------------------------------------------------------- Sessions[]
#define SES_ROWS                            4                       // number of rows in array Sessions
#define SES_SERVER                          0                       // Broker AccountServer()
#define SES_SYMBOL                          1                       // Symbol
#define SES_TRADE_START                     2                       // Trading Hours Start
#define SES_TRADE_END                       3                       // Trading Hours End
#define SES_CLOSE_START                     4                       // Daily Close Start
#define SES_CLOSE_END                       5                       // Daily Close End
//-------------------------------------------------------------------- Signal()
#define SIG_WAIT                           -2                       // no action
#define SIG_BUY                             0                       // Buy
#define SIG_SELL                            1                       // Sell
//-------------------------------------------------------------------- Pause times
#define WAIT_GBL_INI                       30
 
//+------------------------------------------------------------------+
//| EX4 imports                                                      |
//+------------------------------------------------------------------+
#include <stdlib.mqh>

//+------------------------------------------------------------------+
//|                                                    TimeSuite.mq4 |
//|                                         Copyright � 2008, sx ted |
//|                                               sxted@talktalk.net |
//| Purpose: Library of functions usefull for time calculations and  |
//|          determining if a report is imminent.                    |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2008, sx ted"
#property link      "sxted@talktalk.net"
 
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
#define TIME_ROWS              18                                   // Number of Cities catered for in table
#define TIME_STD_DIFF           0                                   // Standard offset with GMT
#define TIME_DST_DIFF           1                                   // Dailight Saving Time difference compared to GMT
#define TIME_DST_START_MN       2                                   // Dailight Saving Time start month
#define TIME_DST_START_WM       3                                   // Dailight Saving Time start Week index position in
                                                                    // month (1=1st, 2=2nd, 3=3rd, 5=last in the month)
#define TIME_DST_START_DW       4                                   // Dailight Saving Time start day of the week
                                                                    // (0=Sunday)
#define TIME_DST_END_MN         5                                   // Dailight Saving Time end month
#define TIME_DST_END_WM         6                                   // Dailight Saving Time end Week index position in
                                                                    // month (1=1st, 2=2nd, 3=3rd, 5=last in the month)
#define TIME_DST_END_DW         7                                   // Dailight Saving Time end day of the week
                                                                    // (0=Sunday)
 
//+------------------------------------------------------------------+
//| Function..: TimeAt                                               |
//| Parameters: t     - Time to be converted.                        |
//|             sCity - UN Locode to identify the city, for codes    |
//|                     refer to http://www.timegenie.com/city.time/ |
//|             ToGMT - 1=convert time <t> at <sCity> to GMT time,   |
//|                     0=convert GMT time <t> to time of <sCity>.   |
//| Purpose...: Convert a GMT time (current or future) to the time   |
//|             of another City, or vice versa.                      |
//| Returns...: tTime - Time at City <sCity> or -1 if City not found.|
//| Notes.....: Specs: http://webexhibits.org/daylightsaving/g.html  |
//|             Europe start & end at 1 am UTC, Moscow start & end   |
//|             at 2 am local time are ignored as fx market closed   |
//|             on the days that DST start and end.                  |
//|             Brazil rules vary quite a bit from year to year,     |
//|             data table only caters for DST Oct 2007 to Feb 2008. |
//| Example...: // if in Brussels:                                   |
/*+------------------------------------------------------------------+
Print("Time GMT: ",TimeToStr(TimeAt(0,"BEBRU"),TIME_DATE|TIME_SECONDS));
//+-----------------------------------------------------------------*/ 
datetime TimeAt(datetime t=0, string sCity="BEBRU", bool ToGMT=1) {
  int    i, iYear;
  string s[TIME_ROWS]={"BEBRU", // Brussels (Bruxelles), Belgium
                       "NZQEL", // Wellington, New Zealand
                       "AUSYD", // Sydney, New South Wales, Australia
                       "JPTYO", // Tokyo, Japan
                       "CNBJS", // Beijing (Peking), China
                       "HKHKG", // Hong Kong, Hong Kong S.A.R. (Xianggang)
                       "INBOM", // Mumbai (Bombay), India
                       "RUMOW", // Moscow (Moskva), Russian Federation
                       "SARUH", // Riyadh, Saudi Arabia
                       "ZAJNB", // Johannesburg, Gauteng, South Africa
                       "DEBER", // Berlin, Germany (Deutschland)
                       "FRPAR", // Paris, France
                       "GBLON", // London, GB (UK)
                       "GBGNW", // Greenwich, GB (UK)
                       "BRRIO", // Rio de Janeiro, Brazil
                       "USNYC", // New York, United States
                       "CAOTT", // Ottawa, Ontario, Canada
                       "CAVAN"  // Vancouver, British Columbia, Canada
                      };
  double d[TIME_ROWS][8]={ +1,+1,03,5,0,10,5,0, // BEBRU Brussels (Bruxelles), Belgium
                          +12,+1,09,5,0,04,1,0, // NZQEL Wellington, New Zealand
                          +10,+1,10,5,0,03,5,0, // AUSYD Sydney, New South Wales, Australia
                           +9,+0,00,0,0,00,0,0, // JPTYO Tokyo, Japan
                           +8,+0,00,0,0,00,0,0, // CNBJS Beijing (Peking), China
                           +8,+0,00,0,0,00,0,0, // HKHKG Hong Kong, Hong Kong S.A.R. (Xianggang)
                         +5.5,+0,00,0,0,00,0,0, // INBOM Mumbai (Bombay), India
                           +3,+1,03,5,0,10,5,0, // RUMOW Moscow (Moskva), Russian Federation
                           +3,+0,00,0,0,00,0,0, // SARUH Riyadh, Saudi Arabia
                           +2,+0,00,0,0,00,0,0, // ZAJNB Johannesburg, Gauteng, South Africa
                           +1,+1,03,5,0,10,5,0, // DEBER Berlin, Germany (Deutschland)
                           +1,+1,03,5,0,10,5,0, // FRPAR Paris, France
                           +0,+1,03,5,0,10,5,0, // GBLON London, GB (UK)
                            0,+0,00,0,0,00,0,0, // GBGNW Greenwich, GB (UK)
                           -3,+1,10,3,0,02,3,0, // BRRIO Rio de Janeiro, Brazil
                           -5,+1,03,2,0,11,1,0, // USNYC New York, United States
                           -5,+1,03,2,0,11,1,0, // CAOTT Ottawa, Ontario, Canada
                           -8,+1,03,2,0,11,1,0  // CAVAN Vancouver, British Columbia, Canada
                        };
  for(i=0; i<TIME_ROWS; i++) {
    if(s[i]==sCity) break;
  }
  if(i>=TIME_ROWS) return(-1);
  if(t==0) t=TimeCurrent();
  t=t+d[i][TIME_STD_DIFF]*PERIOD_H1*60*(1+ToGMT*(-2)); // Standard offset
  if(d[i][TIME_DST_DIFF]==0) return(t);
  if(d[i][TIME_DST_START_MN] > d[i][TIME_DST_END_MN]) iYear=PERIOD_D1*365*60;
  if((t >= TimeExSpecs(t,d[i][2],d[i][3],d[i][4])) && (t <= TimeExSpecs(t+iYear,d[i][5],d[i][6],d[i][7]))) t=t+d[i][TIME_DST_DIFF]*PERIOD_H1*60*(1+ToGMT*(-2));
  return(t); 
}
 
//+------------------------------------------------------------------+
//| Function..: TimeExSpecs                                          |
//| Parameters: t  - Time to be used as template, usually the local  |
//|                  time.                                           |
//|             MN - Month to be used in the return value.           |
//|             WM - Week index position in the month (1=1st, 2=2nd, |
//|                  3=3rd, 4=4th, 5=last). Specifying 5 retrieves   |
//|                  the last occurence of the day of the week which |
//|                  has the value of parameter DW.                  |
//|             DW - Day of the week (0=Sunday,1,2,3,4,5,6=Saturday).|
//|             HH - Hour (24 hours) to be used in the return value. |
//|             MM - Minutes to be used in the return value.         |
//| Purpose...: Derive next report time from specs.                  |
//| Returns...: Report time.                                         |
//| Example...: // Determine start of daylight saving time in Europe |
//|             // which occurs in March on the last Sunday          |
//|             datetime tStartDST=TimeExSpecs(TimeLocal(),3,5,0);   |
//|             Print("DST in Europe ", TimeToStr(tStartDST));       |
//+------------------------------------------------------------------+ 
datetime TimeExSpecs(datetime t, int MN=1, int WM=1, int DW=0, int HH=0, int MM=0) {
  t=StrToTime(StringConcatenate(TimeYear(t),".",MN,".1 ",HH,":",MM));
  while(TimeDayOfWeek(t) != DW) t=t+PERIOD_D1*60;
  if(WM > 1) t=t+(PERIOD_W1*(WM-1))*60;
  if(TimeMonth(t) != MN) t=t-PERIOD_W1*60;
  return(t);
}
 
//+------------------------------------------------------------------+
//| Function..: TimeInDailyClose                                     |
//| Parameters: tTime   - Time to be checked.                        |
//|             sStart  - Start time of the range for which <tTime>  |
//|                       is to be verified, using the notation      |
//|                       "HH:MM" where: HH=Hour and MM=Minute.      |
//|             sEnd    - End time of the range, ditto.              |
//|             iAdjust - Number of minutes to adjust range times:   |
//|                       (defaults to 15 minutes)                   |
//|                       <sStart> is adjusted to start earlier by   |
//|                                the value of <iAdjust> and,       |
//|                       <sEnd>   is adjusted to start later by     |
//|                                the number of <iAdjust> minutes.  |
//| Purpose...: Determine if the time <tTime> falls in the Daily     |
//|             Close period of an exchange defined by <sStart> and  |
//|             <sEnd>.                                              |
//| Returns...: Zero if time <tTime> is not in the range defined by  |
//|             the <sStart> and <sEnd> parameters, or the number of |
//|             seconds remaining before the end of the range period |
//|             <sEnd> is reached.                                   |
//| Example...: if(TimeInDailyClose(TimeCurrent(),"23:15","00:00")>0)|
//|               return(-1);  // can not trade                      |
//| See Also..: TimeInRange()                                        |
//+------------------------------------------------------------------+
int TimeInDailyClose(datetime tTime, string sStart, string sEnd, int iAdjust=15) {
  if(sStart==sEnd) return(0);
  datetime tStart=(tTime/86400)*86400 + StrToInteger(StringSubstr(sStart,0,2))*3600 + StrToInteger(StringSubstr(sStart,3,2))*60 - iAdjust*60;
  datetime tEnd=(tTime/86400)*86400 + StrToInteger(StringSubstr(sEnd,0,2))*3600 + StrToInteger(StringSubstr(sEnd,3,2))*60 + iAdjust*60;
  
  if(tTime<=tEnd && tStart>tEnd) return(tEnd-tTime);
  if(tEnd<tStart) tEnd=tEnd+86400*7;
  if(tTime>=tStart && tTime<=tEnd) return(tEnd-tTime);
  return(0);
}
 
//+------------------------------------------------------------------+
//| Function..: TimeInRange                                          |
//| Parameters: tTime   - Time to be checked.                        |
//|             sStart  - Start time of the range for which <tTime>  |
//|                       is to be verified, using the notation      |
//|                       "D HH:MM" where:                           |
//|                       D=Day of week (0-Sunday,1,2,3,4,5,6),      |
//|                       HH=Hour and MM=Minute.                     |
//|             sEnd    - End time of the range, ditto.              |
//|             iAdjust - Number of minutes to adjust range times:   |
//|                       (defaults to 15 minutes)                   |
//|                       <sStart> is adjusted to start later by the |
//|                                number of <iAdjust> minutes and,  |
//|                       <sEnd>   is adjusted to start earlier by   |
//|                                the number of <iAdjust> minutes.  |
//| Purpose...: Determine if the time <tTime> falls in the period    |
//|             between <sStart> and <sEnd>.                         |
//| Returns...: Zero if time <tTime> is not in the range defined by  |
//|             the <sStart> and <sEnd> parameters, or the number of |
//|             seconds remaining before the end of the range period |
//|             <sEnd> is reached.                                   |
//| Notes.....: Adjust times to finish earlier or re-start later to  |
//|             avoid possible volatility if using the function to   |
//|             check for trading session, or use <iAdjust>.         |
//| Example...: // Sleep during week end!                            |
//|           Sleep(TimeInRange(TimeAt()),"0 23:30","5 21:30")*1000);|  
//| See Also..: TimeInDailyClose()                                   |
//+------------------------------------------------------------------+
int TimeInRange(datetime tTime, string sStart, string sEnd, int iAdjust=15) {
  int      iDOW=TimeDayOfWeek(tTime);
  datetime tStart=(tTime/86400)*86400 + (StrToInteger(StringSubstr(sStart,0,1)) - iDOW)*86400 + StrToInteger(StringSubstr(sStart,2,2))*3600 + StrToInteger(StringSubstr(sStart,5,2))*60 + iAdjust*60;
  datetime tEnd=(tTime/86400)*86400 + (StrToInteger(StringSubstr(sEnd,0,1)) - iDOW)*86400 + StrToInteger(StringSubstr(sEnd,2,2))*3600 + StrToInteger(StringSubstr(sEnd,5,2))*60 - iAdjust*60;
  
  if(tTime<=tEnd && tStart>tEnd) return(tEnd-tTime);
  if(tEnd<tStart) tEnd=tEnd+86400*7;
  if(tTime>=tStart && tTime<=tEnd) return(tEnd-tTime);
  return(0);
}
//+------------------------------------------------------------------+
 
//+------------------------------------------------------------------+
//| global variables to program:                                     |
//+------------------------------------------------------------------+
color  gcColorScheme[]={Blue,                                       //  0 OP_BUY       Buying position
                        Red,                                        //  1 OP_SELL      Selling position
                        DeepSkyBlue,                                //  2 OP_BUYLIMIT  Buy limit pending position
                        Magenta,                                    //  3 OP_SELLLIMIT Sell limit pending position
                        DeepSkyBlue,                                //  4 OP_BUYSTOP   Buy stop pending position
                        Magenta                                     //  5 OP_SELLSTOP  Sell stop pending position
                       };
double Options[]={10,                                               // 0 OPT_ORDER_RETRY_ATTEMPTS
                  true,                                             // 1 OPT_PRINT_MSG
                  1                                                 // 2 OPT_TRADE_PAUSE_SECONDS
                 },
       Price[2],                                                    // Ask and Bid prices of Symbol/Instrument
       gdPoint,
       gdSpread,
       gdStopLevel;
int    giAction,                                                    // return value of OrderProcess() and determines next move
       giCmd,                                                       // trade type or order function number
       giDigits,
       giError,                                                     // value of last call to GetLastError()
                                                                    // from within functions of OrdersSuite.mqh
       giExtra=2,                                                   // extra padding for MarketInfo(sSymbol,MODE_STOPLEVEL)
       giSlippage,
       giX[]={-1,1,1,-1};                                           // Price[] multipliers;
 
//-------------------------------------------------------------------- Trading hours and Daily close times:
string Sessions[SES_ROWS][6]={                                      /*
  For each broker, if several are used, the first row is to detail
  the default trading times for all Spot Currencies, then followed
  on the next rows with the exceptions. The trading hours are read
  by the function TimeInRange() using the notation "D HH:MM" where
  D=Day of week (0-Sunday,1,2,3,4,5,6), HH=Hour and MM=Minute.
+-------------------+---------+---------+---------+-------+-------+
|                   |         | Trading | Trading | Daily | Daily |
| Broker            |         | Hours   | Hours   | Close | Close |
| AccountServer()   | Symbol  | Start   | End     | Start | End   |
+-------------------+---------+---------+---------+-------+-------+ */
"DEFAULT"           ,"DEFAULT","0 23:00","5 23:00","00:00","00:00", // default when only currencies used by broker (in which case this would be the only row in the array)
"MIG-Demo"          ,"DEFAULT","0 23:00","5 23:00","00:00","00:00", // broker default for all Spot Currencies
"MIG-Demo"          ,"XAGUSD" ,"1 00:00","5 23:00","23:15","00:00", // broker exception for Spot Silver commodity
"MIG-Demo"          ,"XAUUSD" ,"1 00:00","5 23:00","23:15","00:00"  // broker exception for Spot Gold commodity
};
 
//+------------------------------------------------------------------+
//| Function..: CmdToStr                                             |
//+------------------------------------------------------------------+
string CmdToStr(int cmd) {
  switch(cmd) {
    case OP_BUY:       return("OP_BUY");
    case OP_SELL:      return("OP_SELL");
    case OP_BUYLIMIT:  return("OP_BUYLIMIT");
    case OP_SELLLIMIT: return("OP_SELLLIMIT");
    case OP_BUYSTOP:   return("OP_BUYSTOP");
    case OP_SELLSTOP:  return("OP_SELLSTOP");
    case ORD_CLOSE:    return("ORD_CLOSE");
    case ORD_CLOSEBY:  return("ORD_CLOSEBY");
    case ORD_DELETE:   return("ORD_DELETE");
    case ORD_MODIFY:   return("ORD_MODIFY");
    default:           return("cmd unknown");
  }
}
 
//+------------------------------------------------------------------+
//| return error description                                         |
//+------------------------------------------------------------------+
string ErrorDescription2(int error_code) {
  switch(error_code) {
    //---- codes returned from OrdersSuite.mqh functions
    case  0: return("");
    case -1: return("EA terminated by user");      
    case -2: return("retry period exceeded");      
    case -3: return("Account/MarketInfo=0");       
    case -4: return("wait for session to open");   
    case -5: return("Margin call soon!!!");        
    case -6: return("OrderSend2 cancelled - market volatile!"); 
    default: return(ErrorDescription(error_code));
  }
}
 
//+------------------------------------------------------------------+
//| Function..: ErrorSet                                             |
//+------------------------------------------------------------------+
int ErrorSet(int iError, int iReturn=0, int iAction=-100) {
  giError=iError;
  if(iAction!=-100) giAction=iAction;
  return(iReturn);
}
 
//+------------------------------------------------------------------+
//| Function..: GetLastError2                                        |
//| Purpose...: Replacement for the GetLastError() function when     |
//|             using the functions in this library.                 |
//| Returns...: Operates like GetLastError().                        |
//| Notes.....: The global variable giError may be used directly.    |
//|             The test for an error must now be: if(giError != 0)  |
//|             as new negative numbers have been added, refer to    |
//|             the ErrorDescription2() function.                    |
//+------------------------------------------------------------------+
int GetLastError2() {
  int iError=giError;
  giError=0;
  return(iError);
}
 
//+------------------------------------------------------------------+
//| Function..: GetMarketInfo                                        |
//| Parameters: sSymbol - Symbol for trading.                        |
//| Purpose...: Retrieve certain data about a Symbol/Instrument.     |
//| Returns...: bool Success.                                        |
//+------------------------------------------------------------------+
bool GetMarketInfo(string sSymbol) {
  RefreshRates();
  gdPoint=MarketInfo(sSymbol,MODE_POINT);
  /*debug*/ if(GetLastError()==4106) return(Msg(0,"GetMarketInfo: unknown symbol?",sSymbol));
  if(CompareDoubles(gdPoint,0.0)) return(false);
  Price[0]=MarketInfo(sSymbol,MODE_ASK);
  Price[1]=MarketInfo(sSymbol,MODE_BID);
  giDigits=MarketInfo(sSymbol,MODE_DIGITS);
  giSlippage=MathMax((Price[0]-Price[1])/gdPoint,5);
  gdSpread=MarketInfo(sSymbol,MODE_SPREAD);
  gdStopLevel=MarketInfo(sSymbol,MODE_STOPLEVEL);
  return(Price[0]>0.0 && Price[1]>0.0 && giDigits>0 && gdSpread>0 && gdStopLevel>0);
}
 
//+------------------------------------------------------------------+
//| Function..: GlobalVariable                                       |
//| Parameters: sName     - Global variable name.                    |
//|             bGetValue - 1=Get current value.                     |
//|                         0=Set new value.                         |
//|             dValue    - The new numeric value.                   |
//| Purpose...: Create a global variable if not present and/or       |
//|             initialise/change it's value for multiple EA's.      | 
//| Returns...: dValue - Value of the global variable named <sName>  |
//|                      or -1 if an error occurs. Call the function |
//|                      GetLastError2() to get the detailed error   |
//|                      information or inspect the value of giError.|
//| Notes.....: Used for status of "_OpenNewOrders" & "_StopTrading".|
//|             Assumes sole use or that a SemLock() has first been  |
//|             set.                                                 |
//| Example...: if(SemLock()) {                                      |
//|               GlobalVariable("_OpenNewOrders",0,OrdersOpen()==0);|
//|               SemUnlock();                                       |
//|             }                                                    |
//+------------------------------------------------------------------+
double GlobalVariable(string sName, bool bGetValue=1, double dValue=0) {
  int iStartWaitingTime=GetTickCount();
  
  while(true) {
    if(IsStopped()) return(ErrorSet(-1,-1));
    if(GetTickCount()-iStartWaitingTime > WAIT_GBL_INI*1000) return(ErrorSet(-2,-1));
    if(bGetValue && GlobalVariableCheck(sName)) dValue=GlobalVariableGet(sName); 
    else GlobalVariableSet(sName,dValue);
    if(GetLastError()==0) return(dValue);
    Sleep(100);
    continue;
  }
}
 
//+------------------------------------------------------------------+
//| Function..: Lots                                                 |
//| Thank you.: http://championship.mql4.com/2006/news/8 for formula |
//|             and also to GwadaTradeBoy for the additional code    |
//|             "Code - Optimisation des Lots.mq4".                  |
//| Parameters: sSymbol         - Security symbol, defaults to chart |
//|                               symbol.                            |
//|             dMaxRisk        - Maximum risk, 5% by default.       |
//|             dDecreaseFactor - Decrease lot size if last order    |
//|                               was closed with a loss. The default|
//|                               is to decrease the lot size.       |
//|                               Pass value 0.0 for no decrease.    |
//| Purpose...: Calculate number of lots permissible with current    |
//|             equity value of the account.                         |
//| Returns...: Amount of lots allowed or 0.0 if an error occured.   |
//| Example...:                                                      |
//+------------------------------------------------------------------+
double Lots(string sSymbol="", double dMaxRisk=0.05, double dDecreaseFactor=3) {
  if(sSymbol=="") sSymbol=Symbol();
  double dLotMin =MarketInfo(sSymbol,MODE_MINLOT),
         dLotMax =MarketInfo(sSymbol,MODE_MAXLOT),
         dLotStep=MarketInfo(sSymbol,MODE_LOTSTEP),
         dLotSize=MarketInfo(sSymbol,MODE_LOTSIZE), vol;
  int    iOrders =HistoryTotal()-1, iLosses, iAccountLeverage=AccountLeverage(), i;
  
  if(dLotMin<=0.0 || dLotMax<=0.0 || dLotStep<=0.0 || dLotSize<=0.0 || iAccountLeverage<=0) return(ErrorSet(-3,0,ACT_CAN_RETRY)); 
  vol=NormalizeDouble(AccountFreeMargin()*dMaxRisk*iAccountLeverage/dLotSize,2);
  if(dDecreaseFactor>0) {
    for(i=iOrders; i>=0; i--) {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==false) return(0.0);
      if(OrderType()>OP_SELL) continue;
      if(OrderProfit()>0) break;
      if(OrderProfit()<0) iLosses++;
    }
    if(iLosses>1) vol=NormalizeDouble(vol-vol*iLosses/dDecreaseFactor,1);
  }    
  vol=NormalizeDouble(vol/dLotStep,0)*dLotStep;
  if(vol<dLotMin) vol=dLotMin;
  if(vol>dLotMax) vol=dLotMax;
  return(vol);
}       
 
//+------------------------------------------------------------------+
//| Function..: Msg                                                  |
//| Parameters: iReturn - value to be returned.                      |
//|             xValue1 - boolean/double/integer/string value 1.     |
//|             xValue2 - boolean/double/integer/string value 2.     |
//|             xValue3 - boolean/double/integer/string value 3.     |
//|             xValue4 - boolean/double/integer/string value 4.     |
//| Purpose...: Display error or information messages if toggle      |
//|             Options[OPT_PRINT_MSG] has been set to <true>.       |
//| Returns...: iReturn.                                             |
//| Notes.....: DateTime value to be formatted using TimeToStr().    |
//| Examples..: Msg(0,"Error:",giError,ErrorDescription2(giError));  |
//|             Msg(1,Symbol(),"bought at",Ask);                     |
//+------------------------------------------------------------------+
int Msg(int iReturn, string sVal1, string sVal2="", string sVal3="", string sVal4="") {
  if(Options[OPT_PRINT_MSG]==true) Print(sVal1," ",sVal2," ",sVal3," ",sVal4);
  return(iReturn);
}
 
//+------------------------------------------------------------------+
//| Function..: Option                                               |
//| Parameters: iIndex    - Index number of the option.              |
//|             bGetValue - 1=Get value of the option (the default), |
//|                         0=Set new value for the option.          |
//|             dValue    - New value for the option.                |
//| Purpose...: Set values which are to be set differently for an EA |
//|             or a script, but not using an external parameter.    |
//| Returns...: dValue - Current value of the option.                |
//| Notes.....: Index | Option                       | Default value |
//|             ------+------------------------------+-------------- |
//|              0    | OPT_ORDER_RETRY_ATTEMPTS     | 10            |
//|              1    | OPT_PRINT_MSG                | true          |
//|              2    | OPT_TRADE_PAUSE_SECONDS      | 1             |
//| Example...: if(Option[OPT_PRINT_MSG]==true) Print("Hi");         |
//+------------------------------------------------------------------+
double Option(int iIndex=0, bool bGetValue=true, double dValue=0) {
  double dCurValue=Options[iIndex];
  if(!bGetValue) Options[iIndex]=dValue;
  return(dCurValue); 
}
 
//+------------------------------------------------------------------+
//| Function..: OrderCheckFunds                                      |
//| Parameters: sSymbol - Symbol for trading operation.              | 
//|             cmd     - Operation type. It can be either OP_BUY or |
//|                       OP_SELL.                                   | 
//|             dLots   - Number of lots.                            |
//| Purpose...: Determine if there are sufficient funds to open a    |
//|             new position.                                        |
//| Returns...: bSuccess. In case of failure function GetLastError2()|
//|             may be called or the global variable giError may be  |
//|             used to retrieve the error number.                   |   
//+------------------------------------------------------------------+
bool OrderCheckFunds(string sSymbol, int cmd, double dLots) {
  double dVal=AccountEquity(), 
         dVal2=AccountFreeMarginCheck(sSymbol,cmd,dLots);
 
  if(CompareDoubles(dVal,0.0) || CompareDoubles(dVal2,0.0)) return(ErrorSet(-3,0,ACT_EXIT_START));
  if((dVal/(dVal-dVal2)) < 0.50) return(ErrorSet(-5,0,ACT_EXIT_START));
  return(1);
}
          
//+------------------------------------------------------------------+
//| Function..: OrderClose2                                          |
//| Parameters: Same as OrderClose()                                 |
//| Purpose...: Close opened order like MT4 function OrderClose()    |
//|             but with error handling.                             |
//| Returns...: bool Success.                                        |
//+------------------------------------------------------------------+
bool OrderClose2(int iTicket, double dLots, double dPrice, int iSlippage, color cColor=CLR_NONE) {
  return(OrderProcess(ORD_CLOSE,OrderSymbol(),iTicket,cColor,0,dLots,dPrice,iSlippage));
}   
 
//+------------------------------------------------------------------+
//| Function..: OrderCloseBy2                                        |
//| Parameters: Same as OrderCloseBy()                               |
//| Purpose...: Close opened order like MT4 function OrderCloseBy()  |
//|             but with error handling.                             |
//| Returns...: bool Success.                                        |
//+------------------------------------------------------------------+
bool OrderCloseBy2(int iTicket, int iOpposite, color cColor=CLR_NONE) {
  return(OrderProcess(ORD_CLOSEBY,OrderSymbol(),iTicket,cColor,iOpposite));
}  
 
//+------------------------------------------------------------------+
//| Function..: OrderCloseByRetracement                              |
//| Parameters: iRetracement - Retracement value in points.          |
//| Purpose...: Close an order that is in profit which has retraced  |
//|             by the amount of points specified by <iRetracement>. | 
//| Returns...: 0 - Function OrderProcess() returned an error.       |
//|             1 - Success, and order closed.                       |
//|             2 - Order is not in profit or, has not retraced, or  |
//|                 <iRetracement> is less than 1.                   |
//| Notes.....: The order must have been previously selected by the  |
//|             OrderSelect() function.                              |
//| Example...: OrderCloseByRetracement(Retracement);                |
//+------------------------------------------------------------------+
int OrderCloseByRetracement(int iRetracement) {
  string sSymbol=OrderSymbol();
  if(iRetracement<1) return(2);
  if(!GetMarketInfo(sSymbol)) return(ErrorSet(-3,0,ACT_CAN_RETRY));
  int x=iBarShift(sSymbol,PERIOD_M1,OrderOpenTime());
  
  if(OrderProfit()>0) {  
    if((OrderType()==OP_BUY  && Price[OP_SELL] <= iHigh(sSymbol,PERIOD_M1,iHighest(sSymbol,PERIOD_M1,MODE_HIGH,x)) - iRetracement*gdPoint) ||
       (OrderType()==OP_SELL && Price[OP_SELL] >= iLow(sSymbol,PERIOD_M1,iLowest(sSymbol,PERIOD_M1,MODE_LOW,x)) + iRetracement*gdPoint) )
      return(OrderProcess(ORD_CLOSE,sSymbol,OrderTicket(),CLR_NONE,0,OrderLots(),Price[1-OrderType()],giSlippage));
  }
  return(2);
}
 
//+------------------------------------------------------------------+
//| Function..: OrderDelete2                                         |
//| Parameters: Same as OrderDelete()                                |
//| Purpose...: Cancel pending order like MT4 function OrderDelete() |
//|             but with error handling.                             |
//| Returns...: bool Success.                                        |
//+------------------------------------------------------------------+
bool OrderDelete2(int iTicket, color cColor=CLR_NONE) {
  return(OrderProcess(ORD_DELETE,OrderSymbol(),iTicket,cColor));
}
 
//+------------------------------------------------------------------+
//| Function..: OrderError                                           |
//+------------------------------------------------------------------+
void OrderError() {
  if(giCmd<=OP_SELLSTOP) Print("Order:new  Error:",giError," ",ErrorDescription2(giError));
  else Print("Order:",OrderTicket(),"  Error:",giError," ",ErrorDescription2(giError));
}
 
//+------------------------------------------------------------------+
//| Function..: OrderModify2                                         |
//| Parameters: Same as OrderModify()                                |
//| Purpose...: Modification of characteristics for the previously   |
//|             opened position or pending orders like MT4 function  |
//|             OrderModify() but with error handling.               | 
//| Returns...: bool Success.                                        |
//| Notes.....: The order is submitted for modification only if      |
//|             there is a change in one or more of the values.      |
//|             If error 130 ERR_INVALID_STOPS is triggered then the |
//|             order is cancelled if it is pending, or the market   |
//|             order is closed.                                     |
//+------------------------------------------------------------------+
bool OrderModify2(int iTicket, double dPrice, double dSL, double dTP, datetime tExpire, color cColor=CLR_NONE) {
  if(!GetMarketInfo(OrderSymbol())) return(ErrorSet(-3,0,ACT_CAN_RETRY));
  if(OrderType()<=OP_SELL && ((NormalizeDouble(OrderStopLoss()-dSL,8)!=0) || (NormalizeDouble(OrderTakeProfit()-dTP,8)!=0))) return(OrderProcess(ORD_MODIFY,OrderSymbol(),iTicket,cColor,0,0,OrderOpenPrice(),0,NormalizeDouble(dSL,giDigits),NormalizeDouble(dTP,giDigits),OrderExpiration()));
  else if(OrderType()>OP_SELL && ((NormalizeDouble(OrderOpenPrice()-dPrice,8)!=0) || (tExpire!=OrderExpiration()))) return(OrderProcess(ORD_MODIFY,OrderSymbol(),iTicket,cColor,0,0,NormalizeDouble(dPrice,giDigits),0,OrderStopLoss(),OrderTakeProfit(),tExpire));
  return(true); // but no modifications
}
 
//+------------------------------------------------------------------+
//| Function..: OrderModifyTS                                        |
//| Parameters: iTrailingStop     - Pass value of external parameter |
//|                                 <TrailingStop> to adjust the     |
//|                                 OrderStopLoss value.             |
//|             bAdjustTakeProfit - If passed as <true> adjusts the  |
//|                                 OrderTakeProfit with the value   |
//|                                 of <iTrailingStop>               |
//| Purpose...: Modify the Stop loss and Take profit levels of an    |
//|             opened position with the value of a TrailingStop.    | 
//| Returns...: 0 - Function OrderProcess() returned an error.       |
//|             1 - Success, and order modified.                     |
//|             2 - Trailing stop can not be adjusted as difference  |
//|                 between market price and the stop loss is less   |
//|                 than the trailing stop value, or the value is 0. |
//| Notes.....: The order must have been previously selected by the  |
//|             OrderSelect() function.                              |
//|             The order is submitted for modification only if      |
//|             there is a change in the StopLoss/TakeProfit level.  |
//|             If error 130 ERR_INVALID_STOPS is triggered then the |
//|             order is cancelled if it is pending, or the market   |
//|             order is closed.                                     |
//| Example...: if(TrailingStop>0) {                                 |
//|               OrderModifyTS(TrailingStop, AdjustTakeProfit);     |
//|             }                                                    |
//+------------------------------------------------------------------+
int OrderModifyTS(int iTrailingStop, bool bAdjustTakeProfit) {
  if(!GetMarketInfo(OrderSymbol())) return(ErrorSet(-3,0,ACT_CAN_RETRY));
  double dSL=OrderStopLoss(), dTP=OrderTakeProfit(), dSL2, dTP2=dTP,
         dTS=(MathMax(gdStopLevel,iTrailingStop)+giExtra)*gdPoint;
 
  if(iTrailingStop>0) {
    if(OrderType()==OP_BUY) {
      if(dSL!=0) dSL2=dSL+dTS*MathFloor((Price[1]-dTS-dSL)/dTS);
      else       dSL2=Price[1]-dTS;
      if(bAdjustTakeProfit && (dTP!=0) && (dSL!=0) && (dTP+dSL2-dSL-Price[1]>=dTS)) dTP2=dTP+(dSL2-dSL);
      if((Price[1]-dSL2>=dTS) && (dSL2>dSL)) return(OrderProcess(ORD_MODIFY,OrderSymbol(),OrderTicket(),CLR_NONE,0,0,OrderOpenPrice(),0,NormalizeDouble(dSL2,giDigits),NormalizeDouble(dTP2,giDigits),OrderExpiration()));
    }
    else {
      if(dSL!=0) dSL2=dSL-dTS*MathFloor((dSL-dTS-Price[0])/dTS);
      else       dSL2=Price[0]+dTS;
      if(bAdjustTakeProfit && (dTP!=0) && (dSL!=0) && (Price[0]-(dTP-(dSL-dSL2))>=dTS)) dTP2=dTP-(dSL-dSL2);
      if((dSL2-Price[0]>=dTS) && ((dSL!=0 && dSL2<dSL) || (dSL==0))) return(OrderProcess(ORD_MODIFY,OrderSymbol(),OrderTicket(),CLR_NONE,0,0,OrderOpenPrice(),0,NormalizeDouble(dSL2,giDigits),NormalizeDouble(dTP2,giDigits),OrderExpiration()));
    }
  }
  return(2);
}
 
//+------------------------------------------------------------------+
//| Function..: OrderOpenPos                                         |
//| Thank you.: KimIV reference http://forum.mql4.com/ru/6688        |
//| Parameters: sym - Symbol/Instrument, defaults to current symbol, |
//|             op  - Order type, (-1 is any type),                  |
//|             mn  - Magic number, (-1 is any Magic number).        |
//| Purpose...: Locate position number of order that was last opened |
//|             in the trading pool.                                 |
//| Returns...: iPosition or -1 (not found).                         |
//+------------------------------------------------------------------+
int OrderOpenPos(string sym="", int op=-1, int mn=-1) {
  datetime t;
  int      i, j=-1, k=OrdersTotal();
 
  if (sym=="") sym=Symbol();
  for (i=0; i<k; i++) {
    if (OrderSelect(i, SELECT_BY_POS, MODE_TRADES)) {
      if (OrderSymbol()==sym) {
        if (op<0 || OrderType()==op) {
          if (mn<0 || OrderMagicNumber()==mn) {
            if (t<OrderOpenTime()) {
              t=OrderOpenTime();
              j=i;
            }
          }
        }
      }
    }
  }
  return(j);
}
 
//+------------------------------------------------------------------+
//| Function..: OrderProcess                                         |
//| Parameters: Are the same as passed by functions OrderClose2(),   |
//|             OrderCloseBy2(), OrderDelete2(), OrderModify2() and  |
//|             OrderSend2().                                        |
//|             Additional optional parameter:                       |
//|             iBatchNum - 0: No batch process (the default).       |
//|                         1: Order part 1 of 2 of a batch process, |
//|                            that signals that the semaphore named |
//|                            "_Trade" (GlobalVariable) is to remain|
//|                            locked after processing the first     |
//|                            order, example for a straddle order.  |
//|                         2: Signals that the semaphore be unlocked|
//|                            after processing order part 2 of 2.   |
//| Purpose...: Process all orders of an EA.                         |
//|             It includes error handling, so as to stay in the     |
//|             brokers good books, but mainly protect our money.    |
//| Returns...: If the call to OrderProcess() was made from function |
//|             OrderSend2() then the iTicket number is returned,    |
//|             for the other ordering functions a boolean value is  |
//|             returned. In case of failure function GetLastError2()|
//|             may be called or the value of the global variable    |
//|             giError may be inspected.                            |
//| Notes.....: If a critical error is encountered while processing  |
//|             an order, the error handler will attempt to close or |
//|             cancel ALL market/pending orders of ALL experts and  |
//|             manual trades and then set the global variable named |
//|             "_StopTrading" to <true> thus preventing any further |
//|             orders to be processed; after correcting the error   |
//|             hit F3 to change the value of the global variable.   |
//|                                                                  |
//|             In case of abnormal termination (caused by the PC or |
//|             the user) the semaphore lock (GlobalVariable) named  |
//|             "_Trade", if present, should be removed by adding on |
//|             the first line of the expert deinit() function the   |
//|             following code:                                      |
/*+------------------------------------------------------------------+       
void deinit() {
  if(GlobalVariableCheck("_Trade")) GlobalVariableDel("_Trade");
}
//+-----------------------------------------------------------------*/                 
//|             As a last resort pressing key F3 gives access to the |
//|             GlobalVariable "_Trade" which is to be deleted, and  |
//|             the other experts closed and rerun.                  |
//|                                                                  |
//|             The global variable giAction returns the severity of |
//|             the error and has the following values:              | 
//|             ACT_SUCCESS           0 No errors encountered, can   |
//|                                     proceed to the next order,   |
//|             ACT_CAN_RETRY        -1 MarketInfo()=0, should exit  |
//|                                     start() function, and retry, |  
//|             ACT_EXIT_START       -2 The EA must exit the start() |
//|                                     function, and retried at the |
//|                                     next tick,                   |
//|             ACT_WAIT_FOR_SESSION -3 Exit start() to avoid error  |
//|                                     133 if currencies and        |
//|                                     commodities traded at the    |
//|                                     same time, but with different|
//|                                     daily close times,           |
//|             ACT_EXIT_EA          -4 Program logic/System failure,|
//|                                     the EA should be stopped.    |
//+------------------------------------------------------------------+
int OrderProcess(int cmd, string sSymbol, int iTicket=0, color cColor=CLR_NONE, int iOpposite=0,
                 double dLots=0, double dPrice=0, int iSlippage=0, double dSL=0, double dTP=0,
                 datetime tExpire=0, string sComment="", int iMagic=0, int iBatchNum=0) {
  bool     ok;
  int      i, iCnt, iPreviousCmd, iReturn, iOrders;
  string   sServer=AccountServer(), sMsg="";
  datetime t=TimeCurrent();
    
  giAction=ACT_CAN_RETRY;
  giCmd=cmd;
  //------------------------------------------------------------------ validate
  if(cmd <= OP_SELLSTOP) {
    iReturn=-1;
    if((GlobalVariable("_OpenNewOrders")==false) && (OrdersOpen()==0)) GlobalVariable("_OpenNewOrders", 0, true);
    if(GlobalVariable("_OpenNewOrders")==false) {
      giAction=ACT_EXIT_START;                                      // sin bin!
      return(iReturn);
    }
    if(!OrderCheckFunds(sSymbol,cmd%2,dLots)) return(iReturn);
  }
  if(GlobalVariable("_StopTrading")==true) giAction=ACT_EXIT_EA;    // enforce warning received from broker  
  else {
    OrderResume();                                                  // check if EA is to be delayed
    //---------------------------------------------------------------- check if trading session open for symbol and avoid error 133 when trading currencies and commodities simultaneously
    for(i=SES_ROWS-1; i>=0; i--) {
      if(Sessions[i][SES_SERVER]==sServer   && Sessions[i][SES_SYMBOL]==sSymbol)   break;
      if(Sessions[i][SES_SERVER]==sServer   && Sessions[i][SES_SYMBOL]=="DEFAULT") break;
      if(Sessions[i][SES_SERVER]=="DEFAULT" && Sessions[i][SES_SYMBOL]=="DEFAULT") break;
    }
    if(TimeInRange(t,Sessions[i][SES_TRADE_START],Sessions[i][SES_TRADE_END])==0 || TimeInDailyClose(t,Sessions[i][SES_CLOSE_START],Sessions[i][SES_CLOSE_END])>0) ErrorSet(-4,iReturn,ACT_WAIT_FOR_SESSION);
  }
  //------------------------------------------------------------------ get sole use of the trading thread
  while((iBatchNum<2) && GlobalVariableCheck("_Trade")) i+=0;
  GlobalVariableSet("_Trade",1);
  //------------------------------------------------------------------ manage order
  while(giAction == ACT_CAN_RETRY) {
    if(!GetMarketInfo(sSymbol)) {                                   // get latest prices/specs/data
      GlobalVariableDel("_Trade");                                  // unlock semaphore
      return(ErrorSet(-3,iReturn,ACT_CAN_RETRY));
    }
    if(cmd <= OP_SELL)            dPrice=Price[cmd%2];
    t=TimeCurrent();                                                // record time stamp
    //---------------------------------------------------------------- prepare error documentation (if required)
    if(Options[OPT_PRINT_MSG]==true) {
      if(cmd <= OP_SELLSTOP)      sMsg=StringConcatenate("Ask=",Price[0]," Bid=",Price[1]," MarketInfo(",sSymbol,",MODE_STOPLEVEL)=",gdStopLevel," OrderSend(",sSymbol,",",CmdToStr(cmd),",",dLots,",",dPrice,",",iSlippage,",",dSL,",",dTP,",",sComment,",",iMagic,",",tExpire,",",cColor,")");
      else if(cmd == ORD_CLOSE)   sMsg=StringConcatenate("OrderClose(",iTicket,",",dLots,",",dPrice,",",iSlippage,",",cColor,")");
      else if(cmd == ORD_CLOSEBY) sMsg=StringConcatenate("OrderCloseBy(",iTicket,",",iOpposite,",",cColor,")");
      else if(cmd == ORD_DELETE)  sMsg=StringConcatenate("OrderDelete(",iTicket,",",cColor,")");
      else if(cmd == ORD_MODIFY)  sMsg=StringConcatenate("Ask=",Price[0]," Bid=",Price[1]," MarketInfo(",sSymbol,",MODE_STOPLEVEL)=",gdStopLevel," cmd=",CmdToStr(OrderType())," OrderModify(",iTicket,",",dPrice,",",dSL,",",dTP,",",tExpire,",",cColor,")");
    }
    //---------------------------------------------------------------- process order
    if(cmd <= OP_SELLSTOP)        iTicket=OrderSend(sSymbol,cmd,dLots,dPrice,iSlippage,dSL,dTP,sComment,iMagic,tExpire,cColor);
    else if(cmd == ORD_CLOSE)     OrderClose(iTicket,dLots,Price[1-OrderType()],iSlippage,cColor);
    else if(cmd == ORD_CLOSEBY)   OrderCloseBy(iTicket,iOpposite,cColor);
    else if(cmd == ORD_DELETE)    OrderDelete(iTicket,cColor);
    else if(cmd == ORD_MODIFY)    OrderModify(iTicket,dPrice,dSL,dTP,tExpire,cColor);
    iPreviousCmd=cmd;                                               // remember previous command
    //---------------------------------------------------------------- handle errors
    giError=GetLastError();
    switch(giError) {
      case    0: // ERR_NO_ERROR
                 if((cmd <= OP_SELLSTOP) && (OrderSelect(iTicket, SELECT_BY_TICKET)==true)) OrderPrint();
      case  144: // The order was discarded by the client during manual confirmation.
                 giAction=ACT_SUCCESS;
                 break;
      case    1: // ERR_NO_RESULT
      case  136: // ERR_OFF_QUOTES // unconfirmed prices/fast market - usually generates error 130 - ** error handled ok **
                 while(RefreshRates()==false)                       // Before new tick
                 Sleep(1);                                          // Delay in the cycle
                 break;
      case  137: // ERR_BROKER_BUSY 
                 Sleep(5000+MathMod(MathRand(),1000));
                 iCnt=0; // note
                 break;
      case    4: // ERR_SERVER_BUSY
      case    8: // ERR_TOO_FREQUENT_REQUESTS - program logic must be changed
      case  132: // ERR_MARKET_CLOSED
                 OrderResume(TimeCurrent()+3*60);
                 giAction=ACT_EXIT_START;
                 break;
      case    6: // ERR_NO_CONNECTION ** error handled ok **
                 for(i=0; i < 6; i++) {
                   Sleep(5000);
                   if(IsConnected()) break;
                 }
                 giAction=ACT_EXIT_START;
                 break;
      case  128: // ERR_TRADE_TIMEOUT  ** error handled ok ** w/ OrderSend()
      case  142: // Order has been enqueued: server disconnected during op
      case  143: // Order was accepted by the dealer for execution: server disconnected during op.
                 // Ensure that trading operation has not really succeeded  ** error handled ok ** w/ OrderModify()
                 // (a new position has not been opened, or the existing order has not been modified
                 // or deleted, or the existing position has not been closed) before retry
                 ok=false;
                 if(cmd<=OP_SELLSTOP) ok=(OrderOpenPos(sSymbol,cmd,iMagic)>0 && OrderOpenTime()>=t);
                 else if(cmd==ORD_CLOSE || cmd==ORD_CLOSEBY || cmd==ORD_DELETE) ok=(OrderSelect(iTicket,SELECT_BY_TICKET) && (OrderCloseTime() > 0));
                 else if(cmd==ORD_MODIFY) ok=(OrderSelect(iTicket,SELECT_BY_TICKET) && (OrderStopLoss() == dSL && OrderTakeProfit() == dTP && OrderOpenPrice() == dPrice && OrderExpiration() == tExpire));
                 if(ok) giAction=ACT_SUCCESS;
                 else   Sleep(65000);
                 break;
      case  130: // ERR_INVALID_STOPS
                 if(!GetMarketInfo(sSymbol)) {
                   GlobalVariableDel("_Trade");                     // unlock semaphore
                   return(ErrorSet(-3,iReturn,ACT_CAN_RETRY));
                 }
                 if((cmd>OP_SELL) && (cmd<=OP_SELLSTOP)) {
                   cmd=cmd%2; // change to market order request ** error handled ok **
                   if(dSL!=0) dSL=Price[OrderType()%2]-(dPrice-dSL);
                   if(dTP!=0) dTP=Price[OrderType()%2]+(dTP-dPrice);
                 }
                 else if(cmd==ORD_MODIFY) {
                   if(OrderType()>OP_SELL)  cmd=ORD_DELETE; // market volatile!
                   if(OrderType()<=OP_SELL) cmd=ORD_CLOSE;  // market volatile! ** error handled ok **
                   dLots=OrderLots();
                 }
                 Sleep(5000+MathMod(MathRand(),1000));
                 break;
      case  134: // ERR_NOT_ENOUGH_MONEY
                 dLots=ReduceLots(dLots,MarketInfo(sSymbol,MODE_MARGINREQUIRED),MarketInfo(sSymbol,MODE_LOTSTEP));
                 if(dLots>0.0) Sleep(5000);
                 else GlobalVariable("_StopTrading",0,true);
                 break;
      case  129: // ERR_INVALID_PRICE
      case  135: // ERR_PRICE_CHANGED
      case  138: // ERR_REQUOTE // prices out of date or bid & ask mixed up or fast market ** error handled ok **
                 break; 
      case  145: // ERR_TRADE_MODIFY_DENIED
                 Sleep(16000+MathMod(MathRand(),2000));
                 if((OrderType() >OP_SELL) && (iCnt>1)) cmd=ORD_DELETE;
                 if((OrderType()<=OP_SELL) && (iCnt>1)) cmd=ORD_CLOSE;
                 break;
      case  146: // ERR_TRADE_CONTEXT_BUSY ** error handled ok **
                 for(i=-1; i < ERR_146_MAX_TRIES; i++) {
                   Sleep(ERR_146_MIN_SEC*1000+MathMod(MathRand(),(ERR_146_MAX_SEC-ERR_146_MIN_SEC)*1000)); // random delay
                   if(!IsTradeContextBusy()) break;
                 }
                 if(i >= ERR_146_MAX_TRIES) giAction=ACT_EXIT_START;
                 break;
      case  147: // ERR_TRADE_EXPIRATION_DENIED 
                 tExpire=0;
                 break;
      case  148: // ERR_TRADE_TOO_MANY_ORDERS
                 GlobalVariable("_OpenNewOrders",0,false);
                 giAction=ACT_EXIT_START;
                 break;
      case 4107: // ERR_INVALID_PRICE_PARAM - Invalid price ** error handled ok ** with OrderSend() 
                 if(((cmd>OP_SELL) && (cmd<=OP_SELLSTOP)) || (cmd==ORD_MODIFY)) dPrice=NormalizeDouble(dPrice,giDigits);
                 break;
      case 4108: // ERR_INVALID_TICKET can occur with OrderClose(), OrderCloseBy(), OrderDelete() or
                 // OrderModify() when the position has reached it's TP, SL or Expiration time just
                 // before or while the EA has issued the order (for example: processing time for
                 // trade requests can be from 2 to 7 seconds, but the variable Option[OPT_TRADE_PAUSE_SECONDS]
                 // has been set to 10 seconds)
                 giAction=ACT_EXIT_START;
                 iCnt=0; // note
                 break;                                       
      default:   //   2: ERR_COMMON_ERROR - Common error. All attempts to trade must be stopped
                 //      until reasons are clarified. Restart of operation system and client
                 //      terminal will possibly be needed. (While testing and running live, this
                 //      error was only generated by the broker's server).
                 //   3: ERR_INVALID_TRADE_PARAMETERS (program logic)
                 //   5: ERR_OLD_VERSION
                 //  64: ERR_ACCOUNT_DISABLED
                 //  65: ERR_INVALID_ACCOUNT
                 // 131: ERR_INVALID_TRADE_VOLUME (Program logic)
                 // 133: ERR_TRADE_DISABLED
                 // 139: ERR_ORDER_LOCKED // order locked (and can not be cancelled), program logic must be changed
                 // 140: ERR_LONG_POSITIONS_ONLY_ALLOWED
                 // 141: ERR_TOO_MANY_REQUESTS
                 GlobalVariable("_StopTrading",0,true);
                 break;
    }
    //---------------------------------------------------------------- publish errors encountered
    if(giAction != ACT_SUCCESS) {
      Msg(0,"OrderProcess: error",giError,ErrorDescription2(giError),sMsg);
      Msg(0,"OrderProcess: after error Ask:",MarketInfo(sSymbol,MODE_ASK),"Bid:",MarketInfo(sSymbol,MODE_BID));
      if(cmd!=iPreviousCmd) Msg(0,"OrderProcess: changing cmd from",CmdToStr(iPreviousCmd),"to",CmdToStr(cmd));
      if(iCnt>Options[OPT_ORDER_RETRY_ATTEMPTS]) Msg(GlobalVariable("_StopTrading",0,true),"OrderProcess: retries exceeded:");
      if((GlobalVariable("_StopTrading")==true) && (giError!=2)) {
        // attempt to close/cancel ALL the remaining market/pending orders except for ERR_COMMON_ERROR
        iOrders=OrdersTotal()-1;
        for(i=iOrders; i>=0; i--) {
          if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)) {
            if((OrderType()<=OP_SELL) && GetMarketInfo(OrderSymbol())) OrderClose(OrderTicket(),OrderLots(),Price[1-OrderType()],giSlippage);
            else if(OrderType()>OP_SELL)                               OrderDelete(OrderTicket());
            Sleep(2000);
          }
        }
        giAction=Msg(ACT_EXIT_EA,"OrderProcess: severe error - close/cancell ALL orders attempted - EXITING EA!");
        if(OrdersTotal()>0) Alert("Severe error encountered - CLOSE/CANCEL REMAINING ORDERS!!!");
      }
    }
    giCmd=cmd;
    iCnt++;
  }
  //------------------------------------------------------------------ 
  t=TimeLocal();                                                    // avoid error 146 (ERR_TRADE_CONTEXT_BUSY)
  while(TimeLocal()-t < SEM_PAUSE) i+=0;                            // by enforcing a pause in between trades,
  if((giAction==ACT_SUCCESS && iBatchNum!=1) || (giAction!=ACT_SUCCESS))
    GlobalVariableDel("_Trade");                                    // unlock semaphore,
  if(cmd<=OP_SELLSTOP) return(iTicket);                             // return appropriate success/failure values
  return(giAction == ACT_SUCCESS);
}
 
//+------------------------------------------------------------------+
//| Function..: OrderResume                                          |
//| Parameters: tResume - time, based on TimeCurrent(), at which     |
//|                       trading may be resumed.                    |
//|                       If the parameter is not passed then the    |
//|                       execution of the expert is suspended for   |
//|                       the interval between the current time and  |
//|                       the time previously passed to the function.|
//| Purpose...: Get/Set time at which trading may be resumed.        |
//+------------------------------------------------------------------+
void OrderResume(datetime tResume=0) {
  static datetime t;
  
  if(tResume>0) t=MathMax(t,tResume);
  else if(t>TimeCurrent()) Sleep((t-TimeCurrent())*1000);
}
 
//+------------------------------------------------------------------+
//| Function..: OrderSend2                                           |
//| Parameters: Same as OrderSend() except for:                      |
//|             cColor - Color of the opening arrow on the chart.    |
//|                      If the parameter is passed as CLR_DEF the   |
//|                      default color scheme of gcColorScheme[] is  |
//|                      used.                                       | 
//| Purpose...: Open a position or place a pending order like MT4    |
//|             function OrderSend() but with error handling.        |
//| Returns...: iTicket - ticket number assigned to the order by the |
//|             trade server, or -1 if it fails. GetLastError2() may |
//|             be called or giError value retrieved to obtain the   |
//|             error information. The value of the global variable  |
//|             giAction indicates the severity of the error, refer  |
//|             to the notes of OrderProcess().                      |
//| Reference.: http://book.mql4.com/trading/orders                  | 
//| Notes.....: Adjusts the opening price of a pending order if it   |
//|             is too close to the market.                          |
//|             Adjusts StopLoss and TakeProfit if too close to the  |
//|             market.                                              |
//|             Converts pending orders rejected by the server for   |
//|             being too close to market automatically into market  |
//|             orders.                                              |
//|             Prevents trading after severe warnings on system     |
//|             failure or program logic.                            |
//|             Enforces closure of all orders after error 148 has   |
//|             been issued before a new order can be placed.        |
//|             Verifies that funds are sufficient to place an order.|
//|             OrderSend2() is cancelled if the market is volatile. |
//| Cmd types.: OP_BUY       0 Buying position (market order),       |
//|             OP_SELL      1 Selling position (market order),      |
//|             OP_BUYLIMIT  2 Buy limit pending position (counter   |
//|                            trend order placed below the market), |
//|             OP_SELLLIMIT 3 Sell limit pending position (counter  |
//|                            trend order placed above the market), |
//|             OP_BUYSTOP   4 Buy stop pending position             |
//|                            (trend order placed above the market),|
//|             OP_SELLSTOP  5 Sell stop pending position            |
//|                            (trend order placed below the market).|
//+------------------------------------------------------------------+
int OrderSend2(string sSymbol, int cmd, double dLots, double dPrice, int iSlippage, double dSL, double dTP, string sComment="", int iMagic=0, datetime tExpire=0, color cColor=CLR_DEF) {
  int k=cmd%2;                                                      // determine if Buy/Sell type of order to be made
  
  if(cColor==CLR_DEF)                           cColor=gcColorScheme[cmd];
  if(!GetMarketInfo(sSymbol))                   return(ErrorSet(-3,-1,ACT_CAN_RETRY));
  if(NormalizeDouble(Price[0]-Price[1]-gdSpread*gdPoint,8) > 0) return(ErrorSet(-6,-1,ACT_EXIT_START)); // exit if market volatile when opening a position
  if(cmd<=OP_SELL)                              dPrice=Price[k];
  else if(cmd==OP_BUYLIMIT || cmd==OP_SELLSTOP) dPrice=MathMin(dPrice,Price[k]-(gdStopLevel+giExtra)*gdPoint);
  else if(cmd==OP_SELLLIMIT || cmd==OP_BUYSTOP) dPrice=MathMax(dPrice,Price[k]+(gdStopLevel+giExtra)*gdPoint);
  if(dSL!=0) {
    if(k==OP_BUY) dSL=MathMin(dSL,dPrice-(gdStopLevel+gdSpread+giExtra)*gdPoint);
    else          dSL=MathMax(dSL,dPrice+(gdStopLevel+gdSpread+giExtra)*gdPoint);
  }
  if(dTP!=0) {
    if(k==OP_BUY) dTP=MathMax(dTP,dPrice+(gdStopLevel+gdSpread+giExtra)*gdPoint);
    else          dTP=MathMin(dTP,dPrice-(gdStopLevel+gdSpread+giExtra)*gdPoint);
  }
  return(OrderProcess(cmd,sSymbol,-1,cColor,0,dLots,NormalizeDouble(dPrice,giDigits),iSlippage,NormalizeDouble(dSL,giDigits),NormalizeDouble(dTP,giDigits),tExpire,sComment,iMagic));
}
 
//+------------------------------------------------------------------+
//| Function..: OrderSendI                                           |
//| Parameters: Same as OrderSend() except for:                      |
//|             cColor - Color of the opening arrow on the chart.    |
//|                      If the parameter is passed as CLR_DEF the   |
//|                      default color scheme of gcColorScheme[] is  |
//|                      used.                                       |
//|             iSL    - Stop loss level expressed in points.        |
//|             iTP    - Take profit level expressed in points.      |
//| Purpose...: Open a position or place a pending order like MT4    |
//|             function OrderSend() but with error handling.        |
//| Returns...: iTicket - ticket number assigned to the order by the |
//|             trade server, or -1 if it fails. GetLastError2() may |
//|             be called or giError value retrieved to obtain the   |
//|             error information. The value of the global variable  |
//|             giAction indicates the severity of the error, refer  |
//|             to the notes of OrderProcess().                      |
//| Reference.: http://book.mql4.com/trading/orders                  | 
//| Notes.....: Adjusts the opening price of a pending order if it   |
//|             is too close to the market.                          |
//|             Adjusts StopLoss and TakeProfit if too close to the  |
//|             market.                                              |
//|             Converts pending orders rejected by the server for   |
//|             being too close to market automatically into market  |
//|             orders.                                              |
//|             Prevents trading after severe warnings on system     |
//|             failure or program logic.                            |
//|             Enforces closure of all orders after error 148 has   |
//|             been issued before a new order can be placed.        |
//|             Verifies that funds are sufficient to place an order.|
//|             OrderSendI() is cancelled if the market is volatile. |
//| Cmd types.: OP_BUY       0 Buying position (market order),       |
//|             OP_SELL      1 Selling position (market order),      |
//|             OP_BUYLIMIT  2 Buy limit pending position (counter   |
//|                            trend order placed below the market), |
//|             OP_SELLLIMIT 3 Sell limit pending position (counter  |
//|                            trend order placed above the market), |
//|             OP_BUYSTOP   4 Buy stop pending position             |
//|                            (trend order placed above the market),|
//|             OP_SELLSTOP  5 Sell stop pending position            |
//|                            (trend order placed below the market).|
//+------------------------------------------------------------------+
int OrderSendI(string sSymbol, int cmd, double dLots, double dPrice, int iSlippage, int iSL, int iTP, string sComment="", int iMagic=0, datetime tExpire=0, color cColor=CLR_DEF) {
  double dSL, dTP;
  int    k=cmd%2;                                                   // determine if Buy/Sell type of order to be made
  
  if(cColor==CLR_DEF)                           cColor=gcColorScheme[cmd];
  if(!GetMarketInfo(sSymbol))                   return(ErrorSet(-3,-1,ACT_CAN_RETRY));
  if(NormalizeDouble(Price[0]-Price[1]-gdSpread*gdPoint,8) > 0) return(ErrorSet(-6,-1,ACT_EXIT_START)); // exit if market volatile when opening a position
  if(cmd<=OP_SELL)                              dPrice=Price[k];
  else if(cmd==OP_BUYLIMIT || cmd==OP_SELLSTOP) dPrice=MathMin(dPrice,Price[k]-(gdStopLevel+giExtra)*gdPoint);
  else if(cmd==OP_SELLLIMIT || cmd==OP_BUYSTOP) dPrice=MathMax(dPrice,Price[k]+(gdStopLevel+giExtra)*gdPoint);
  if(iSL!=0) dSL=dPrice+MathMax(iSL,gdStopLevel+gdSpread+giExtra)*gdPoint*giX[k];
  if(iTP!=0) dTP=dPrice+MathMax(iTP,gdStopLevel+gdSpread+giExtra)*gdPoint*giX[k+2];
  return(OrderProcess(cmd,sSymbol,-1,cColor,0,dLots,NormalizeDouble(dPrice,giDigits),iSlippage,NormalizeDouble(dSL,giDigits),NormalizeDouble(dTP,giDigits),tExpire,sComment,iMagic));
}
 
//+------------------------------------------------------------------+
//| Function..: OrdersOpen                                           |
//| Parameters: iExpertID - Identity number of the expert. If the    |
//|                         parameter is passed then only the number |
//|                         of orders associated with the expert and |
//|                         bearing OrderMagicNumber() the same as   |
//|                         <iExpertID> is counted, otherwise if the |
//|                         parameter is omitted then the function   |
//|                         will return the total number of orders   |
//|                         for all experts.                         |
//| Purpose...: Determine the number of positions open and orders    |
//|             pending.                                             |
//| Returns...: iOrders - Count of market and pending orders,        |
//|             or -1 if an error was encountered.                   |
//| Notes.....: Anomaly with OrdersTotal() which includes cancelled  |
//|             and closed orders.                                   |
//+------------------------------------------------------------------+
int OrdersOpen(int iExpertID=-1) {
  int iTotal=OrdersTotal(), iOrders, i;
  
  for(i=0; i < iTotal; i++) {                                       // For all orders of the terminal
    if(!OrderSelect(i, SELECT_BY_POS)) return(-1);
    if(iExpertID<0 || OrderMagicNumber()==iExpertID) {
      if(OrderCloseTime()==0) iOrders++;
    }
  }
  return(iOrders);
}
 
//+------------------------------------------------------------------+
//| Function..: ReduceLots                                           |
//| Purpose...: recalculate lots after error 134 ERR_NOT_ENOUGH_MONEY|
//+------------------------------------------------------------------+
double ReduceLots(double dLotPrevious, double dMarginRequired, double dLotStep) {
  if(dMarginRequired<=0.0 || dLotStep<=0.0) return(0.0);
  double vol=NormalizeDouble(AccountFreeMargin()/(dMarginRequired*2),2)*dLotStep;
  if(vol<dLotPrevious && vol>0.0) return(vol);
  return(0.0);
}
 
//+------------------------------------------------------------------+
//| Function..: SemLock                                              |
//| Thank You.: Andrey Khatimlianskyi for his code & expos? on how to|
//|             handle semaphore locks http://articles.mql4.com/141  |
//|             Adapted for OrdersSuite.mqh                          |
//| Parameters: iID    - Unique number identifying the Expert or a   |
//|                      process within the expert, or a different   |
//|                      number for each chart when using the same   |
//|                      expert.                                     |
//|                      Number to be in the range 1 to one million. |
//|             iRetry - Number of seconds to wait for the semaphore |
//|                      to be unlocked.                             |
//|             iPause - Pause (in seconds) required to be enforced  |
//|                      after the semaphore is unlocked. Example a  |
//|                      Server might require a pause of 3 seconds   |
//|                      in between placing or closing orders.       |
//| Purpose...: Obtain the sole handle for trading via the use of a  |
//|             semaphore and enforce a pause between trades of the  |
//|             expert(s) as directed by the value of <iPause> and   |
//|             avoid error 146 (ERR_TRADE_CONTEXT_BUSY).            |
//|             When the semaphore is locked it acquires the value   |
//|             of <iID>, otherwise the value indicates the time     |
//|             that the semaphore was last accessed.                |
//| Returns...: bool Success - The global variable SEM was assigned  |
//|                            with value <iID>,                     |
//|                  False   - The semaphore could not be locked,    |
//|                            the error number can be obtained from |
//|                            GetLastError2() or giError.           |
//| Notes.....: In case of abnormal termination (caused by the PC or |
//|             the user) the semaphore lock, if present, should be  |
//|             removed by adding on the first line of the expert    |
//|             deinit() function the following code (replacing iID  |
//|             with the number identifying the expert or process):  |
/*+------------------------------------------------------------------+       
                if(!IsTesting() &&
                GlobalVariableSetOnCondition(SEM_ID,TimeLocal(),iID))
                Print("semaphore unlocked");
//+-----------------------------------------------------------------*/                 
//|             As a last resort pressing key F3 gives access to the |
//|             GlobalVariable "_ThreadUsedBy" which can be either   |
//|             deleted or modified, and the other experts closed    |
//|             and rerun.                                           |
//|             To allow a script to close all orders promptly while |
//|             other experts are running, do not call SemLock() from|
//|             within the script.                                   |
//+------------------------------------------------------------------+
bool SemLock(int iID=1, int iRetry=30, int iPause=SEM_PAUSE) {
  giError=0;
  GetLastError();
  if(IsTesting()) return(true);
  int    iStartWaitingTime=GetTickCount(), i;
  double dSemVal;
  
  while(true) {
    if(IsStopped()) return(Msg(ErrorSet(-1),"SemLock() Error: -1",ErrorDescription2(-1)));
    if(GetTickCount()-iStartWaitingTime > iRetry*1000) return(ErrorSet(-2)); // note error msg not printed
    if(GlobalVariableCheck(SEM_ID)) break;
    else {
      giError=GetLastError();
      if(giError!=0) {
        Sleep(Msg(100,"SemLock() GlobalVariableCheck Error:",giError,ErrorDescription2(giError)));
        continue;
      }
    }
    if(GlobalVariableSet(SEM_ID,iID ) > 0 ) return(ErrorSet(0,1));
    else {
      giError=GetLastError();
      if(giError!=0) {
        Sleep(Msg(100,"SemLock() GlobalVariableSet Error:",giError,ErrorDescription2(giError)));
        continue;
      }
    }
  }
  while(true) {
    if(IsStopped()) return(Msg(ErrorSet(-1),"SemLock() Error: -1",ErrorDescription2(-1))); 
    if(GetTickCount()-iStartWaitingTime > iRetry*1000) return(ErrorSet(-2)); // note error msg not printed
    dSemVal=GlobalVariableGet(SEM_ID);
    if(dSemVal==0) {
      giError=GetLastError();
      Sleep(Msg(100,"SemLock() GlobalVariableGet Error:",giError,ErrorDescription2(giError)));
      continue;
    }
    if(dSemVal > _MILLION && GlobalVariableSetOnCondition(SEM_ID, iID, dSemVal)) {
      while(TimeLocal()-dSemVal < iPause) i+=0; // enforce the trade pause  
      return(ErrorSet(0,1));
    }
    else {
      // if not, 2 reasons for it are possible: SemLock() has the value of another <iID> (then one has to wait),
      // or an error occurred (this is what we will check)
      giError=GetLastError();
      if(giError!=0) {
        Msg(0,"SemLock() GlobalVariableSetOnCondition Error:",giError,ErrorDescription2(giError));
        continue;
      }
    }
    // if there is no error, it means that 0 < SemLock() < _MILLION (another expert is trading),
    // then display information and wait...
    Comment(StringConcatenate("Thread locked by ",dSemVal));
    Sleep(1000);
    Comment("");
  }
}
 
//+------------------------------------------------------------------+
//| Function..: SemUnlock                                            |
//| Thank You.: Andrey Khatimlianskyi for his code & expos? on how to|
//|             handle semaphore locks http://articles.mql4.com/141  |
//|             Adapted for OrdersSuite.mqh                          |     
//| Parameters: iID    - Unique number identifying the Expert or a   |
//|                      process within the expert.                  |
//|                      Number to be in the range 1 to one million. |
//|             iRetry - Number of seconds to wait for the semaphore |
//|                      to be unlocked.                             |
//| Purpose...: Release sole handle for trading by the EA.           |
//|             The semaphore global variable <SEM_ID> is created if |
//|             it does not exist.                                   |
//| Returns...: bool Success - The semaphore was unlocked.           |
//|                  False   - The semaphore could not be unlocked,  |
//|                            the error number can be obtained from |
//|                            GetLastError2() or giError.           |
//+------------------------------------------------------------------+
bool SemUnlock(int iID=1, int iRetry=30) {
  giError=0;
  GetLastError();
  if(IsTesting()) return(true); 
  int iStartWaitingTime=GetTickCount();
  
  while(true) {
    if(IsStopped()) return(Msg(ErrorSet(-1),"SemUnlock() Error: -1",ErrorDescription2(-1)));
    if(GetTickCount()-iStartWaitingTime > iRetry*1000) return(ErrorSet(-2)); // note error msg not printed
    if(GlobalVariableCheck(SEM_ID)) {
      if(GlobalVariableGet(SEM_ID) > _MILLION) return(ErrorSet(0,1));
      return(ErrorSet(0,GlobalVariableSetOnCondition(SEM_ID,TimeLocal(),iID)));
    }
    if(GlobalVariableSet(SEM_ID,TimeLocal()) > 0) return(ErrorSet(0,1));
    else {
      giError=GetLastError();
      if(giError!=0) Msg(0,"SemUnlock() GlobalVariableSet Error:",giError,ErrorDescription2(giError));
    }
    Sleep(100);
  }
}
 
//+------------------------------------------------------------------+
//| Function..: SeqNum                                               |
//| Parameters: None.                                                |
//| Purpose...: Generate a sequential number.                        |
//| Returns...: dSeqNum - next sequence number or -1 if an error     |
//|                       occured, which can be determined by        |
//|                       calling GetLastError2() or giError.        |
//| Notes.....: MT4 keeps the value of the global variable at the    |
//|             client terminal for 4 weeks since the last access.   |                        
//|             Use SeqNum() to generate a unique identity for each  |
//|             order (and passed via parameter <magic> number, or   |
//|             converted to a string and passed via parameter       |
//|             <comment> to the OrderSend2() function) as the trade |
//|             servers of some brokers do modify the ticket number  |
//|             of a pending order when it changes to a market order.|
//|             The same sequence number could, for example, be used |
//|             to identify the two positions of a straddle order.   |
//|             Use SemLock() and SemUnlock() if the sequence number |
//|             is shared across experts.                            |
//| Example...: if(SemLock()) {                                      |
//|               double dSeqNum=SeqNum();                           |
//|               SemUnlock();                                       |
//|             }                                                    |
//+------------------------------------------------------------------+
double SeqNum() {
  double dSeqNum=1;
  
  if(GlobalVariableCheck("_SequenceNumber")) {
    dSeqNum=GlobalVariableGet("_SequenceNumber")+1;
    if(dSeqNum==1) dSeqNum=-1;
  }
  if((dSeqNum>0) && (GlobalVariableSet("_SequenceNumber",dSeqNum) == 0)) dSeqNum=-1;
  if(dSeqNum==-1) ErrorSet(GetLastError());
  return(dSeqNum);
}
//+------------------------------------------------------------------+